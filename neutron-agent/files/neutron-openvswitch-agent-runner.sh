#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -ex

if [ -n "$NEUTRON_OVS_LOCAL_IP_SUBNET" ]; then
  # TODO: Support IPv6 link-local addresses. For IPv6 link-local addresses
  if ! ovs_local_ip=$(ip a | grepcidr "$NEUTRON_OVS_LOCAL_IP_SUBNET" | grep -Po '((?<=inet\W)\d+\.\d+\.\d+\.\d+|(?<=inet6\W)[a-f\d:]+)'); then
    echo "No interface found matching the subnet '$NEUTRON_OVS_LOCAL_IP_SUBNET'."
    exit 1
  fi

  # shellcheck disable=SC2086
  if [ "$(echo "$ovs_local_ip" | wc -w)" -gt 1 ]; then
    echo "More than one matching interfaces found for the subnet '$NEUTRON_OVS_LOCAL_IP_SUBNET'."
    exit 1
  fi
else
  ovs_local_ip=${NODE_IP}
fi

echo "[ovs]" > /tmp/ipoverwrite.conf
echo "local_ip = ${ovs_local_ip}" >> /tmp/ipoverwrite.conf

cat /tmp/ipoverwrite.conf

neutron-openvswitch-agent --config-file /etc/neutron/neutron.conf --config-file /tmp/ipoverwrite.conf
