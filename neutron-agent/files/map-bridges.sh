#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -euxo pipefail

while read -r map; do
    if [ -z "$map" ]; then
        continue
    fi
    bridge=$(echo "$map" | cut -d \; -f 1)
    iface=$(echo "$map" | cut -d \; -f 2)
    echo "Creating Bridge ${bridge}"
    ovs-vsctl --db=unix:/run/openvswitch/db.sock --no-wait --may-exist add-br "${bridge}"
    echo "Connecting Bridge ${bridge} to interface ${iface}"
    ovs-vsctl --db=unix:/run/openvswitch/db.sock --no-wait --may-exist add-port "${bridge}" "${iface}"
    echo "Setting interface ${iface} to up"
    ip link set dev "${iface}" up
    echo "Done with Bridge ${bridge}"
done < /etc/neutron/bridge_mappings
