<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# Neutron-agent

The docker image for the openstack neutron service.
This image is designed for the neutron agents and not for the server.
It is sufficient to run the neutron-openvswitch-agent and the
neutron-metadata-agent. Other agents might derive from this image.

## License

[Apache 2](LICENSE.txt)
