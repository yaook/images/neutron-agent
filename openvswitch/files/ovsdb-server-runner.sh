#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -ex
# shellcheck disable=SC2124
COMMAND="${@:-start}"

OVS_DB=/run/openvswitch/conf.db
OVS_PID=/run/openvswitch/ovsdb-server.pid
OVS_SOCKET=/run/openvswitch/db.sock

function start () {
  mkdir -p /run/openvswitch
  if [[ ! -e "${OVS_DB}" ]]; then
    ovsdb-tool create "${OVS_DB}"
  fi
  exec ovsdb-server ${OVS_DB} --pidfile=${OVS_PID} --remote=punix:${OVS_SOCKET}
}

function stop () {
  PID=$(cat $OVS_PID)
  ovs-appctl -T1 -t /run/openvswitch/ovsdb-server."${PID}".ctl exit
}

$COMMAND
